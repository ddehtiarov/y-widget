'use strict';

/*jshint esversion: 6 */
document.addEventListener('DOMContentLoaded', function () {
	'use strict';

	var widgetButton = document.getElementById('widget-button');
	var widgetDialog = document.getElementById('widget-dialog');
	var closeButton = document.getElementById('close-button');

	widgetButton.addEventListener('click', function () {
		if (widgetDialog.className.indexOf("widget-dialog--active") === -1) {
			widgetDialog.className = 'widget-dialog widget-dialog--active';
		} else {
			widgetDialog.className = 'widget-dialog';
		}
	});

	closeButton.addEventListener('click', function () {
		widgetDialog.className = 'widget-dialog';
	});

	/*DRAG MODAL START*/
	widgetDialog.onmousedown = function (e) {

		var coords = getCoords(widgetDialog);
		var shiftX = e.pageX - coords.left;
		var shiftY = e.pageY - coords.top;

		function getCoords(elem) {
			var box = elem.getBoundingClientRect();
			return {
				top: box.top + pageYOffset,
				left: box.left + pageXOffset
			};
		}

		widgetDialog.style.position = 'absolute';
		moveAt(e);

		widgetDialog.style.zIndex = 1000;

		function moveAt(e) {
			widgetDialog.style.left = e.pageX - shiftX + 'px';
			widgetDialog.style.top = e.pageY - shiftY + 'px';
		}

		document.onmousemove = function (e) {
			moveAt(e);
		};

		widgetDialog.onmouseup = function () {
			document.onmousemove = null;
			widgetDialog.onmouseup = null;
		};
	};

	/*DISABLE NATIVE DRAG AND DROP*/
	widgetDialog.ondragstart = function () {
		return false;
	};
	/*DISABLE NATIVE DRAG AND DROP*/

	/*DRAG MODAL START*/
}); //DOMContentLoaded close